
#ifndef ANGELSTOW_DOCUMENTUSINGINTERFACE_H
#define ANGELSTOW_DOCUMENTUSINGINTERFACE_H

#include <QtPlugin>

namespace ange {
namespace angelstow {

class IDocument;

/**
 * The interface for the plugins that access the document
 */
class DocumentUsingInterface {

protected:
    ~DocumentUsingInterface() {};

public:

    /**
     * New document has been loaded, or existing document fundamentally redone.
     */
    virtual void documentReset(IDocument* document) = 0;

};

}
} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::DocumentUsingInterface, "dk.ange.angelstow.plugin.using.document")

#endif // ANGELSTOW_DOCUMENTUSINGINTERFACE_H
