#ifndef ANGE_ANGELSTOW_MOVE_H
#define ANGE_ANGELSTOW_MOVE_H

#include "stowtype.h"
// #include <QDebug>

class container_move_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace vessel {
class Slot;
}
}

namespace ange {
namespace angelstow {

/**
 * This class is meant to communicate container moves between Angelstow and plugins.
 * Pains have been taken to avoid the virtual function pointer and virtual method,
 * to keep the class small and fast, while still keeping it reasonable complete.
 *
 * It is simpler than container_move_t, in particular it doesn't contain a
 * pointer to the container it is moving.
 */
class Move {
public:
    /**
     * container moved from source to target in call.
     * @param target spot container is moved to. If null, it is moved to outside vessel
     * @param call when the container is moved.
     * @param type the stowage type (non-placed, macro-placed, micro-placed)
     */
    Move(const ange::vessel::Slot* target,
         const ange::schedule::Call* call,
         const StowType& type)
        : m_target(target),
          m_call(call),
          m_type(type) {
    }

    /**
     * Construct dummy, useless move
     */
    Move() : m_target(0L), m_call(0L), m_type(ange::angelstow::NonPlacedType) {}


    /**
     * @return call where the container is moved
     */
    const ange::schedule::Call* call() const {
        return m_call;
    }

    /**
     * @return copy of the move in the given call
     */
    Move inOtherCall(const ange::schedule::Call* call) const {
        return Move(m_target, call, m_type);
    }

    /**
     * @return the place the container was moved to (null = Quay)
     */
    const ange::vessel::Slot* slot() const {
        return m_target;
    }

    /**
     * @return the stow type
     */
    StowType type() const {
        return m_type;
    }

    bool operator==(const Move& rhs) const {
        return (m_type == rhs.m_type && m_target == rhs.m_target && m_call == rhs.m_call);
    }

protected:
    const ange::vessel::Slot* m_target;
    const ange::schedule::Call* m_call;
    StowType m_type;

private:
    friend class ::container_move_t;
};

}
} // end of namespaces

#endif // ANGE_ANGELSTOW_MOVE_H
