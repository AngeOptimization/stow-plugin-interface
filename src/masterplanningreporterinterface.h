#ifndef ANGELSTOW_MASTERPLANNINGREPORTERINTERFACE_H
#define ANGELSTOW_MASTERPLANNERREPORTERINTERFACE_H

#include <QtPlugin>
#include <QString>

namespace ange {
namespace schedule {
class Call;
}
}

namespace ange {
namespace angelstow {

/**
 * The interface for the plugins that add data to the master planning debugging
 * report
 */
class MasterPlanningReporterInterface {

protected:
    ~MasterPlanningReporterInterface() {};

public:
    /**
     * Return a header for the master planning report. This is HTML that will be wrapped in h2 tags.
     */
    virtual QString headerForMasterPlanningReport() = 0;

    /**
     * Return a section for the master planning report in HTML. Use h3 and lesser for headers.
     */
    virtual QString sectionForMasterPlanningReport(const ange::schedule::Call* call) = 0;

};

}
} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::MasterPlanningReporterInterface, "dk.ange.angelstow.MasterPlanningReporterInterface")

#endif // ANGELSTOW_MASTERPLANNERREPORTERINTERFACE_H
