#ifndef ANGE_ANGELSTOW_PROBLEMADDERINTERFACE_H
#define ANGE_ANGELSTOW_PROBLEMADDERINTERFACE_H


namespace ange {
namespace angelstow {
class Problem;

/**
 * Class that can handle adding problems. Likely a thing for a plugin to *receive* thru ProblemAddingInterface
 */
class ProblemAdderInterface {
    public:
        /**
         * Add a problem (that can be removed by deleting it)
         * The problem adder does not take ownership of the problem.
         * \param problem the problem to add/track/show
         */
        virtual void addProblem(Problem* problem) = 0;
    protected:
        virtual ~ProblemAdderInterface() { };
};
}
}

#endif // ANGE_ANGELSTOW_PROBLEMADDERINTERFACE_H
