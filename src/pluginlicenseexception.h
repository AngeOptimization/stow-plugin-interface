#ifndef ANGELSTOW_PLUGINLICENSEEXCEPTION_H
#define ANGELSTOW_PLUGINLICENSEEXCEPTION_H

#include <QString>

namespace ange {
namespace angelstow {

/**
 * Exception thrown when the license is expired or the check fails for some other reason.
 * Throw this exception from the plugin constructor.
 */
class PluginLicenseException {

public:

    /**
     * Create exception with reason for license check failure.
     */
    PluginLicenseException(const QString& reason) : m_reason(strdup(reason.toUtf8().constData())) { };

    /**
     * Copy constructor is used by throw
     */
    PluginLicenseException(const PluginLicenseException& other) : m_reason(strdup(other.m_reason)) { };

    virtual ~PluginLicenseException() throw() {
        free(m_reason);
    };

    /**
     * The reason the license check failed.
     */
    QString reason() const {
        return QString::fromUtf8(m_reason);
    }

private:
    // Defensively stored in char* as the exception is thrown across plugin interface
    char* m_reason;

};

}
} // namespace

#endif // ANGELSTOW_PLUGINLICENSEEXCEPTION_H
