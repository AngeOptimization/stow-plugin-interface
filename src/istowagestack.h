#ifndef ANGELSTOW_ISTOWAGE_STACK_H
#define ANGELSTOW_ISTOWAGE_STACK_H

#include <QObject>

#include <ange/units/units.h>
#include <ange/vessel/blockweight.h>

namespace ange {
namespace vessel {
class StackSupport;
}
namespace containers {
class Container;
}
}

namespace ange {
namespace angelstow {

class IStowageStack : public QObject {

public:

    explicit IStowageStack(QObject* parent = 0): QObject(parent) {}

    virtual QList<const ange::containers::Container*> containersOnSupport(const ange::vessel::StackSupport* stackSupport) const = 0;

    /**
     * @return the block weight for a given container, empty block weight if the containers is not in the stack
     */
    virtual ange::vessel::BlockWeight containerBlockWeight(const ange::containers::Container* container) const = 0;

};

}} // end of namespaces

#endif // ANGELSTOW_ISTOWAGE_STACK_H
