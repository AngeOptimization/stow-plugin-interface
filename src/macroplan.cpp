#include "macroplan.h"
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/compartment.h>
#include <stdexcept>

using ange::schedule::Call;
using ange::vessel::Compartment;

namespace ange {
namespace angelstow {

MacroPlan::MacroPlan(const ange::schedule::Schedule* schedule, QObject* parent)
    : super(parent),
    m_schedule(schedule),
    m_firstModifiedCall(schedule->calls().at(0))
{
    setObjectName("MacroStowage");
    Q_ASSERT(schedule);
    connect(schedule, &ange::schedule::Schedule::callAboutToBeRemoved, this , &MacroPlan::debugCheckCallIsNotAssigned);
    connect(schedule, &ange::schedule::Schedule::callAboutToBeRemoved, this , &MacroPlan::updateFirstModifiedOnCallRemoved);
    connect(schedule, &ange::schedule::Schedule::rotationChanged, this, &MacroPlan::validateAll);
}

static const MacroPlan::MacroStowageElement* findElement(MacroPlan::MacroStowageSequence sequence,
                                                            const Call* call) {
    Q_FOREACH (const MacroPlan::MacroStowageElement& element, sequence) {
        if (element.load == call) {
            return &element;
        }
        if (element.load->lessThan(call)) {
            if (call->lessThan(element.discharge)) {
                return &element;
            }
        } else {
            break; // Performance optimization, stop looking when we have passed the query call
        }
    }
    return 0;
}

const Call* MacroPlan::dischargeCall(const Compartment* compartment, const Call* call) const {
    const MacroStowageElement* element = findElement(m_macroStowage.value(compartment), call);
    if (element) {
        return element->discharge;
    } else {
        return 0;
    }
}

const Call* MacroPlan::loadCall(const Compartment* compartment, const Call* call) const {
    const MacroStowageElement* element = findElement(m_macroStowage.value(compartment), call);
    if (element) {
        return element->load;
    } else {
        return 0;
    }
}

MacroPlan::Conflicts MacroPlan::conflicts(const Compartment* compartment, const Call* load, const Call* discharge) {
    MacroStowageElement newElement(load, discharge);
    Conflicts conflicts(0);
    Q_FOREACH (const MacroStowageElement& element, m_macroStowage.value(compartment)) {
        conflicts |= newElement.conflict(element);
    }
    return conflicts;
}

QString MacroPlan::callSwapIsLegal(const Call* firstCall, const Call* secondCall) const {
    Q_ASSERT(firstCall->distance(secondCall) == 1);
    for (QHash<const Compartment*, MacroStowageSequence>::const_iterator it = m_macroStowage.begin();
         it != m_macroStowage.end(); ++it) {
        bool dischargeInFirst = false;
        Q_FOREACH (const MacroStowageElement& element, it.value()) {
            // Check for reversed load/discharge
            if (element.load == firstCall && element.discharge == secondCall) {
                return QString("Master planning reversed in %1").arg(it.key()->objectName());
            }
            // Check for overlap. Happens when an element has discharge in firstCall and an (other) has load in secondCall
            if (element.discharge == firstCall) {
                dischargeInFirst = true;
            }
            if (dischargeInFirst && element.load == secondCall) {
                return QString("Master planning overlap in %1").arg(it.key()->objectName());
            }
        }
    }
    return QString();
}

void MacroPlan::insertAllocation(const Compartment* compartment, const Call* load, const Call* discharge) {
    Q_ASSERT(conflicts(compartment, load, discharge) == MacroPlan::NoConflicts);

    // Take the item out by reference (or create it) in order to be able to easily manipulate it
    MacroStowageSequence& sequence = m_macroStowage[compartment];
    sequence << MacroStowageElement(load, discharge);
    qSort(sequence);
    validate(compartment);

    emit compartmentChanged(compartment);
    changedAtCall(load);
}

void MacroPlan::removeAllocation(const Compartment* compartment, const Call* load, const Call* discharge) {
    MacroStowageSequence& sequence = m_macroStowage[compartment];
    int removeCount = sequence.removeAll(MacroStowageElement(load, discharge));
    Q_ASSERT(removeCount == 1); Q_UNUSED(removeCount);
    if (sequence.isEmpty()) {
        m_macroStowage.remove(compartment);
    }
    validate(compartment);

    emit compartmentChanged(compartment);
    changedAtCall(load);
}

MacroPlan::MacroStowageElement::MacroStowageElement(const Call* l, const Call* d)
    : load(l), discharge(d)
{
    Q_ASSERT(load->lessThan(discharge));
}

bool MacroPlan::MacroStowageElement::operator==(const MacroPlan::MacroStowageElement& other) const {
    return load == other.load && discharge == other.discharge;
}

bool MacroPlan::MacroStowageElement::operator<(const MacroPlan::MacroStowageElement& other) const {
    return load->distance(other.load) > 0;
}

MacroPlan::Conflict MacroPlan::MacroStowageElement::conflict(const MacroPlan::MacroStowageElement& other) const {
    if (load == other.load) {
        if (discharge == other.discharge) {
            return MacroPlan::SameAllocation;
        } else {
            return MacroPlan::SameLoadOtherDischarge;
        }
    }
    if (!(load->lessThan(other.discharge))) { // this.load >= other.discharge
        return MacroPlan::NoConflicts;
    }
    if (!(other.load->lessThan(discharge))) { // other.load >= this.discharge
        return MacroPlan::NoConflicts;
    }
    return MacroPlan::OtherConflict;
}

bool MacroPlan::macroStowageEmpty() const {
    return m_macroStowage.isEmpty();
}

void MacroPlan::validate(const Compartment* compartment) const {
    // Q_ASSERTS does nothing if QT_NO_DEBUG is defined
#if !defined(QT_NO_DEBUG)
    const MacroStowageSequence& list = m_macroStowage.value(compartment);
    const Call* previousDisch = 0;
    const Call* previousLoad = 0;
    Q_FOREACH (const MacroStowageElement & p, list) {
        Q_ASSERT(p.discharge != p.load);
        Q_ASSERT(p.load->lessThan(p.discharge));
        Q_ASSERT(previousLoad != p.load);
        Q_ASSERT(previousDisch != p.discharge);
        if (previousDisch) {
            Q_ASSERT(previousDisch->lessThan(p.load) || previousDisch == p.load);
            Q_ASSERT(previousDisch->lessThan(p.discharge));
        }
        if (previousLoad) {
            Q_ASSERT(previousLoad->lessThan(p.load));
            Q_ASSERT(previousLoad->lessThan(p.discharge));
        }
        previousDisch = p.discharge;
        previousLoad = p.load;
    }
#else
    Q_UNUSED(compartment);
#endif
}

void MacroPlan::validateAll() {
    Q_FOREACH (const Compartment * compartment, m_macroStowage.keys()) {
        validate(compartment);
    }
}

void MacroPlan::debugCheckCallIsNotAssigned(Call* callToRemove) {
#if !defined(QT_NO_DEBUG)
    Q_FOREACH(const MacroStowageSequence & list, m_macroStowage) {
        Q_FOREACH(const MacroStowageElement & p, list) {
            Q_ASSERT(p.discharge != callToRemove);
            Q_ASSERT(p.load != callToRemove);
        }
    }
    validateAll();
#else
    Q_UNUSED(callToRemove);
#endif
}

QList< MacroPlan::MacroStowageAllocation > MacroPlan::allAllocations() const {
    QList<MacroStowageAllocation> allocations;
    Q_FOREACH(const Compartment* compartment, m_macroStowage.keys()) {
        Q_FOREACH(const MacroStowageElement& element, m_macroStowage.value(compartment)) {
            MacroStowageAllocation allocation;
            allocation.compartment = compartment;
            allocation.load = element.load;
            allocation.discharge = element.discharge;
            allocations << allocation;
        }
    }
    return allocations;
}

const Call* MacroPlan::firstModifiedCall() const {
    return m_firstModifiedCall;
}

void MacroPlan::setFirstModifiedCall(const Call* call) {
    m_firstModifiedCall = call;
}

void MacroPlan::changedAtCall(const Call* call) {
    if(m_firstModifiedCall->distance(call) < 0) {
        m_firstModifiedCall = call;
    }
}

void MacroPlan::updateFirstModifiedOnCallRemoved(Call* callToRemove) {
    if(m_firstModifiedCall == callToRemove) {
        m_firstModifiedCall = m_schedule->next(callToRemove);
        Q_ASSERT(m_firstModifiedCall);
    }

}

}
}

// QDebug operator<<(QDebug str, const ange::angelstow::MacroStowage::MacroStowageElement& element) {
//     return str << "(" << element.load << "," << element.discharge << ")";
// }

#include "macroplan.moc"
