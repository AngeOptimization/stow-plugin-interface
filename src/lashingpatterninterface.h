#ifndef ANGE_ANGELSTOW_LASHINGPATTERNINTERFACE_H
#define ANGE_ANGELSTOW_LASHINGPATTERNINTERFACE_H

#include <QList>

namespace ange {
namespace containers {
class Container;
}
namespace vessel {
class BayRowTier;
class LashingRod;
}
}

namespace ange {
namespace angelstow {

/**
 * A lashing pattern can return the lashing rods for each container onboard the vessel
 * for a given stowage in a given call.
 */
class LashingPatternInterface {

public:

    virtual ~LashingPatternInterface() {}

    /**
     * Name of lashing pattern, e.g. "UASC A13"
     */
    virtual QString name() const = 0;

    /**
     * The list of lashing rods attached to the given slot. The lashing pattern owns the LashingRod objects.
     */
    virtual QList<const ange::vessel::LashingRod*> lashingRods(const ange::containers::Container* container) const = 0;

    /**
     * Tells if there are cell guides in the given slot.
     */
    virtual bool hasCellGuides(ange::vessel::BayRowTier bayRowTier) const = 0;

};

}} // namespace ange::angelstow

#endif // ANGE_ANGELSTOW_LASHINGPATTERNINTERFACE_H
