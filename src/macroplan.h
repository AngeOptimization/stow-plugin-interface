#ifndef MACROSTOWAGE_H
#define MACROSTOWAGE_H

#include <QObject>
#include <QHash>
#include "stowplugininterface_export.h"

namespace ange {
namespace vessel {
class Compartment;
}
namespace schedule {
class Call;
class Schedule;
}
}

namespace ange {
namespace angelstow {
/**
 * Macro stowage support
 */
class STOWPLUGININTERFACE_EXPORT MacroPlan : public QObject {
    typedef QObject super;
    Q_OBJECT
public:
    /**
     * @param schedule is only used for debug
     */
    MacroPlan(const ange::schedule::Schedule* schedule, QObject* parent);

    /**
     * @param call Query in this call
     * @param compartment Compartment to query for
     * @return the discharge call planned in the given call/compartment, null if nothing is planned
     */
    const ange::schedule::Call* dischargeCall(const ange::vessel::Compartment* compartment,
                                                const ange::schedule::Call* call) const;

    /**
     * @param call Query in this call
     * @param compartment Compartment to query for
     * @return the load call planned in the given call/compartment, null if nothing is planned
     */
    const ange::schedule::Call* loadCall(const ange::vessel::Compartment* compartment,
                                           const ange::schedule::Call* call) const;

    /**
     * Types of conflicts that can exist between allocations in a compartment
     */
    enum Conflict {
        NoConflicts = 0x0,
        SameAllocation = 0x1,
        SameLoadOtherDischarge = 0x2,
        OtherConflict = 0x4
    };
    Q_DECLARE_FLAGS(Conflicts, Conflict);

    /**
     * @return the type of conflicts there is between the given allocation and the current macro stowage
     */
    Conflicts conflicts(const ange::vessel::Compartment* compartment, const ange::schedule::Call* load,
                        const ange::schedule::Call* discharge);

    /**
     * Checks if it is ok two swap two neighbour calls in the schedule
     * Possible reasons for illegality of swap:
     *  - Reverses direction of a macro stowage
     *  - Makes two macro stowages overlap
     * @param firstCall the first call to swap
     * @param secondCall the second call to swap, must be the call after the first call
     * @return reason of why it is not legal, or a null QString if it is legal.
     */
    QString callSwapIsLegal(const ange::schedule::Call* firstCall, const ange::schedule::Call* secondCall) const;

    /**
     * For undo commands
     * Allocate in the macro stow from @param load to @param discharge in @param compartment
     * Only call this if @ref canMacroStow returns true
     */
    void insertAllocation(const ange::vessel::Compartment* compartment, const ange::schedule::Call* load,
                          const ange::schedule::Call* discharge);

    /**
     * For undo commands
     * Clear allocation in the macro stow from @param load to @param discharge in @param compartment
     * Only call this if @ref insertAllocation has been called with the same parameters
     */
    void removeAllocation(const ange::vessel::Compartment* compartment, const ange::schedule::Call* load,
                          const ange::schedule::Call* discharge);
    /**
     * The data type returned by allAllocations;
     */
    struct MacroStowageAllocation {
        const ange::vessel::Compartment* compartment;
        const ange::schedule::Call* load;
        const ange::schedule::Call* discharge;
    };
    /**
     * lists all allocations in a macro stowage
     */
    QList<MacroStowageAllocation>  allAllocations() const;

    // public for debug commands. Not in general exposed.
    struct MacroStowageElement {
        const ange::schedule::Call* load;
        const ange::schedule::Call* discharge;
        MacroStowageElement(const ange::schedule::Call* l, const ange::schedule::Call* d);
        bool operator==(const MacroStowageElement& other) const;
        bool operator<(const MacroStowageElement& other) const;
        Conflict conflict(const MacroStowageElement& other) const;
        friend QDebug operator<<(QDebug str, const MacroStowageElement& element);
    };
    typedef QList<MacroStowageElement> MacroStowageSequence;

    /**
     * Only use for test and debug
     */
    MacroStowageSequence sequenceForCompartment(const ange::vessel::Compartment* compartment) const {
        return m_macroStowage.value(compartment);
    }

    /**
     * @return true if macro stowage is empty, false if not.
     */
    bool macroStowageEmpty() const;

    /**
     * set the first call which the stowage plugin should act upon
     */
    const ange::schedule::Call* firstModifiedCall() const;

    /**
     * set the first call which the stowage plugin should act upon
     */
    void setFirstModifiedCall(const ange::schedule::Call* call);

Q_SIGNALS:
    /**
     * Emitted when macro stowage for compartment has changed
     */
    void compartmentChanged(const ange::vessel::Compartment* compartment);

private Q_SLOTS:
    /**
     * Check that call is not assigned to
     */
    void debugCheckCallIsNotAssigned(ange::schedule::Call* callToRemove);

    /**
     * Updates first modified call when call is removed.
     */
    void updateFirstModifiedOnCallRemoved(ange::schedule::Call* callToRemove);

    /**
     * Validates and asserts all compartments
     */
    void validateAll();

private:
    /**
     * Asserts if structure is inconsistent. Might be compiled out in release mode
     */
    void validate(const ange::vessel::Compartment* compartment) const;

    /**
     * Called when master plan changed at call
     */
    void changedAtCall(const ange::schedule::Call* call);

private:
    /**
     * The structure holding the macro stowage
     */
    QHash<const ange::vessel::Compartment*, MacroStowageSequence> m_macroStowage;
    const ange::schedule::Schedule* m_schedule;
    const ange::schedule::Call* m_firstModifiedCall;
};

}
}

//TODO: reenable this
// QDebug operator<<(QDebug str, const ange::angelstow::MacroStowage::MacroStowageElement& element);

#endif // MACROSTOWAGE_H
