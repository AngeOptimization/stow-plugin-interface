#ifndef ANGE_ANGELSTOW_PROBLEM_H
#define ANGE_ANGELSTOW_PROBLEM_H

#include <QObject>
#include <QIcon>
#include <QString>
#include "problemlocation.h"
#include "stowplugininterface_export.h"


namespace ange {
namespace angelstow {

class ProblemPrivate;
/**
 * Represents a issue in the program
 */
class STOWPLUGININTERFACE_EXPORT Problem : public QObject {
    Q_OBJECT
    public:
        /**
         * Severities for a problem in the stowage.
         */
        enum Severity {
            Notice,
            Warning,
            Error
        };
        /**
         * Constructor
         *
         * Creates a problem with \param severity as severity on a given \param location with \param description as 'short description'
         *
         * Other classes can group problems based on a key constructed from those three components. Please use \ref setDetails for
         * details that aren't relevant to grouping. E.g. equipment ID's.
         */
        Problem(Severity severity, const ProblemLocation& location, const QString& description, QObject* parent = 0);

        /**
         * Sets the details to \param details
         */
        void setDetails(const QString& details);

        /**
         * \return the details of this problem
         */
        QString details() const;

        /**
         * Sets the icon to \icon
         */
        void setIcon(const QIcon& icon);

        /**
         * \return icon for this problem
         */
        QIcon icon() const;

        /**
         * \return location for this problem
         */
        ProblemLocation location() const;

        /**
         * \return the severity of this problem
         */
        Severity severity() const;

        /**
         * \return the description of the problem
         */
        QString description() const;

        static QString severityToString(Severity severity);

        /**
         * dtor
         */
        ~Problem();
    Q_SIGNALS:
        void changed(Problem* problem);
        void destroyed(Problem* problem);
    private:
        QScopedPointer<ProblemPrivate> d;
};

} // namespace angelstow
} // namespace ange

#endif // ANGE_ANGELSTOW_PROBLEM_H
