#ifndef IGUI_H
#define IGUI_H

#include <QObject>

class QAction;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

namespace ange {
namespace angelstow {

class IGui : public QObject {

public:

    explicit IGui(QObject* parent = 0): QObject(parent) {}

    enum Menus {
        FileMenu,
        TaskMenu,
    };

    /**
     * Install action in menu
     */
    virtual void installMenuAction(Menus menu, QAction* action) = 0;

};

}} // end of namespaces

#endif // IGUI_H
