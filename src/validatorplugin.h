#ifndef ANGELSTOW_VALIDATOR_PLUGIN_H
#define ANGELSTOW_VALIDATOR_PLUGIN_H

namespace ange {
namespace vessel {
class Slot;
}
namespace schedule {
class Call;
}
namespace containers {
class Container;
}
namespace angelstow {
class ReasonReserverInterface;
}
}

namespace ange {
namespace angelstow {

class ValidatorPlugin {

public:

    /**
     * Will be called at plugin load, will give the object that can be used for reserving reasons
     */
    virtual void initializeReasonReserver(ange::angelstow::ReasonReserverInterface* reasonReserver) = 0;

    /**
     * A container now occupies said slot, including possibly it's sister. null means it no longer occupies the slot
     * @param container moved container
     * @param call call where container has changed position
     * @param slot slot where container now resides
     * @param old_slot slot where container was previously stowed
     */
    virtual void containerMoved(const ange::containers::Container* container, const ange::schedule::Call* call,
                                const ange::vessel::Slot* slot, const ange::vessel::Slot* old_slot) = 0;

    /**
     * A container was added to the stowage
     */
    virtual void containerAdded(const ange::containers::Container* container) = 0;

    /**
     * A container was removed from the stowage
     */
    virtual void containerRemoved(const ange::containers::Container* container) = 0;

    /**
     * A container was changed in some way
     * (e.g, dangrous goods codes were changed)
     */
    virtual void containerChanged(const ange::containers::Container* container) = 0;

    /**
     * @return true if container would be valid at slot coming from old_slot in call
     */
    virtual bool legalAt(const ange::containers::Container* container, const ange::schedule::Call* call,
                         const ange::vessel::Slot* slot, const ange::vessel::Slot* oldSlot) = 0;

    /**
     * Will be called even if legal_at() returns true.
     * @returns a registered reason (see iapplication::reserve_reason()) or 0 if no reason/not an error
     * (e.g. "IMO conflict") should be returned.
     */
    virtual int whyNot(const ange::containers::Container* container, const ange::schedule::Call* call,
                       const ange::vessel::Slot* slot, const ange::vessel::Slot* oldSlot) = 0;

    /**
     * @returns a human-readable string describing the reason a container should not or could not be stowed.
     */
    virtual QString reasonToString(int reason) const = 0;

};

} //angelstow
} //ange

Q_DECLARE_INTERFACE(ange::angelstow::ValidatorPlugin, "dk.ange.angelstow.plugin.validator")

#endif
