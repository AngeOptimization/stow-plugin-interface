#ifndef ANGE_ANGELSTOW_ISTOWER_H
#define ANGE_ANGELSTOW_ISTOWER_H

#include "move.h"

class QString;
class QUndoCommand;
namespace ange {
namespace schedule {
class Call;
}
namespace containers {
class Container;
}
}

namespace ange {
namespace angelstow {

/**
 * Interface (for plugins) to do a stowage manipulation of some sort.
 * It is an error to have 2 of these at any one time.
 *
 * Unless finalize() is called, the changes will roll back when this is deleted.
 *
 * Changes done are immediately visible in stowage.
 *
 * Upon creation all signals in stowage are blocked by default.
 *
 */
class IStower {

public:

    virtual ~IStower() {};

    /**
     * Replace any moves container has with the list given
     */
    virtual void replaceStow(const ange::containers::Container* container, const QList<Move>& moves) = 0;

    /**
     * When called, the commands are added to the undostack, and no futher commands are possible.
     * Additionally,  stowage signals are unlocked.
     */
    virtual QUndoCommand * takeCommand() = 0;

};

}
}

#endif
