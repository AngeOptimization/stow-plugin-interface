#ifndef ANGE_STOW_STOWAGE_PLUGIN_H_
#define ANGE_STOW_STOWAGE_PLUGIN_H_

#include "stowplugininterface_export.h"
#include "documentusinginterface.h"

#include <QDebug>

namespace ange {
namespace angelstow {
class IStower;
class MacroPlan;
}
namespace schedule {
class Call;
}
namespace vessel {
class Compartment;
}
}

namespace ange {
namespace angelstow {

/**
 * Interface for a stowage plugin
 */
class MasterPlanningPlugin : public DocumentUsingInterface {

public:

    virtual ~MasterPlanningPlugin() {}

    /**
     * Inform the plugin that compartment can accept containers loaded at load and discharged at discharge
     */
    virtual void setUpMacroPlan(const ange::angelstow::MacroPlan* macroStowage) = 0;

    /**
     * Do the actual stowage for @param call, using @param stowing to modify the stowage with the result
     */
    virtual void runStow(const ange::schedule::Call* call, IStower* stowing) = 0;

};

}} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::MasterPlanningPlugin, "dk.ange.angelstow.plugin.masterplanning")

#endif
