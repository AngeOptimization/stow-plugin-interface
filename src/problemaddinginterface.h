#ifndef ANGE_ANGELSTOW_PROBLEMADDINGINTERFACE_H
#define ANGE_ANGELSTOW_PROBLEMADDINGINTERFACE_H

namespace ange {
namespace angelstow {

class ProblemAdderInterface;

/**
 * Interface for plugins that wants to create/show problems
 */
class ProblemAddingInterface {
    public:
        /**
         * Recieve a \param adder to add problems to as needed
         */
        virtual void setProblemAdder(ProblemAdderInterface* adder) = 0;
    protected:
        virtual ~ProblemAddingInterface() { };
};

}
}
Q_DECLARE_INTERFACE(ange::angelstow::ProblemAddingInterface, "dk.ange.angelstow.ProblemAddingInterface")


#endif // ANGE_ANGELSTOW_PROBLEMADDINGINTERFACE_H
