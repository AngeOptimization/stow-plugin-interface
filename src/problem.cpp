#include "problem.h"

namespace ange {
namespace angelstow {

class ProblemPrivate {
    public:
        ProblemPrivate(Problem::Severity severity, const ProblemLocation& location, const QString& description) : m_severity(severity), m_location(location), m_description(description) {
        }
        Problem::Severity m_severity;
        ProblemLocation m_location;
        QString m_description;
        QString m_details;
        QIcon m_icon;
};

Problem::Problem(Problem::Severity severity, const ProblemLocation& location, const QString& description, QObject* parent): QObject(parent), d(new ProblemPrivate(severity, location, description)) {
    Q_ASSERT(location.isLegal());

}

QString Problem::description() const {
    return d->m_description;
}

QString Problem::details() const {
    return d->m_details;
}

QIcon Problem::icon() const     {
    return d->m_icon;
}

ProblemLocation Problem::location() const {
    return d->m_location;
}

void Problem::setDetails(const QString& details)        {
    if(d->m_details != details) {
        d->m_details = details;
        emit changed(this);
    }
}

void Problem::setIcon(const QIcon& icon) {
    d->m_icon = icon;
    emit changed(this);
}

Problem::Severity Problem::severity() const {
    return d->m_severity;
}

Problem::~Problem() {
    emit destroyed(this);
}

static QHash<Problem::Severity, QString> buildSeverityHash() {
    QHash<Problem::Severity, QString> hash;
    hash.insert(Problem::Error, QStringLiteral("Error"));
    hash.insert(Problem::Warning, QStringLiteral("Warning"));
    hash.insert(Problem::Notice, QStringLiteral("Notice"));
    return hash;
}


QString Problem::severityToString(Problem::Severity severity) {
    static QHash<Severity, QString> severityHash = buildSeverityHash();
    Q_ASSERT(severityHash.contains(severity));
    return severityHash.value(severity,QString());
}


} //_namespace angelstow
} // namespace ange

#include "problem.moc"
