#ifndef ANGELSTOW_IDOCUMENT_H
#define ANGELSTOW_IDOCUMENT_H

#include <QObject>

#include "stowtype.h"
#include "stowplugininterface_export.h"

#include <ange/units/units.h>

#include <QList>

namespace ange {
namespace angelstow {
class IStowageStack;
class LashingPatternInterface;
class MacroPlan;
class Move;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class BayRowTier;
class BaySlice;
class Slot;
class Stack;
class Vessel;
}
}

namespace ange {
namespace angelstow {

class STOWPLUGININTERFACE_EXPORT IDocument : public QObject {

    Q_OBJECT

public:

    explicit IDocument(QObject* parent = 0);

    /**
     * @return list of all containers
     */
    virtual QList<ange::containers::Container*> containers() = 0;

    /**
     * @return container by id
     */
    virtual const ange::containers::Container* getContainerById(int id) const = 0;

    /**
     * @return schedule
     */
    virtual const ange::schedule::Schedule* schedule() const = 0;

    /**
     * @return slot of container. Note that container might span the sister slot, too.
     * a return value of 0 means that the container is not onboard at the given call
     */
    virtual const ange::vessel::Slot* slot(const ange::containers::Container* container, const ange::schedule::Call* call) = 0;

    /**
     * Get (one of) the slots occupied by container at call, or null if not on vessel at call
     */
    virtual const ange::vessel::Slot* containerPosition(const ange::containers::Container* container, const ange::schedule::Call* call) const = 0;

    /**
     * Get the correct address in the slot table of the container at call, or the unknown address if not on vessel at call
     */
    virtual const ange::vessel::BayRowTier containerNominalPosition(const ange::containers::Container* container, const ange::schedule::Call* call) const = 0;

    /**
     * @return source (original load call) of container
     */
    virtual const ange::schedule::Call* containerSource(const ange::containers::Container* container) const = 0;

    /**
     * @return the stowage stack for stack at call
     */
    virtual const ange::angelstow::IStowageStack* stowageStackAtCall(const ange::schedule::Call* call, const ange::vessel::Stack* stack) const = 0;

    /**
     * @return destination (ultimate discharge call) of container
     */
    virtual const ange::schedule::Call* containerDestination(const ange::containers::Container* container) const = 0;

    /**
     * @return vector of moves for a container
     */
    virtual QVector<Move> containerMoves(const ange::containers::Container* container) const = 0;

    /**
     * @return weight limit for call and bay slice, or infinity if no limit
     */
    virtual ange::units::Mass weightLimitForBay(const ange::schedule::Call* call, const ange::vessel::BaySlice* bay_slice) const = 0;

    /**
     * @return weight limits set by user at a specific call
     */
    virtual QHash<const ange::vessel::BaySlice*, ange::units::Mass> weightLimitsForBays(const ange::schedule::Call* call) const = 0;

    /**
     * @return document's vessel
     */
    virtual const ange::vessel::Vessel* vessel() const = 0;

    /**
     * @return GM in a specific call
     */
    virtual ange::units::Length gm(const ange::schedule::Call* call) const = 0;

    /**
     * @return container in slot/call, null if no container present
     */
    virtual const ange::containers::Container* slotContents(const ange::schedule::Call* call, const ange::vessel::Slot* slot) const = 0;

    /**
     * @return The lashing pattern for the given call with the current stowage.
     * The caller owns the returned object.
     */
    virtual LashingPatternInterface* lashingPattern(const ange::schedule::Call* call) const = 0;

    /**
     * @return the macro stowage
     */
    virtual const ange::angelstow::MacroPlan* macroStowage() const = 0;

Q_SIGNALS:
    void centerOfMassChanged(const ange::schedule::Call* call);
    void callAdded(ange::schedule::Call* call);
    void callRemoved(ange::schedule::Call* call);

};

}} // namespace

#endif
