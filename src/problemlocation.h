#ifndef PROBLEM_LOCATION_H
#define PROBLEM_LOCATION_H

class QString;
namespace ange {
namespace containers {
class Container;
}
namespace vessel {
class Vessel;
class StackSupport;
}
namespace schedule {
class Call;
}
}

#include <ange/vessel/bayrowtier.h>
#include <QSharedDataPointer>

#include "stowplugininterface_export.h"

namespace ange {
namespace angelstow {

class ProblemLocationData;
/**
 * Locations of problems.
 */
class STOWPLUGININTERFACE_EXPORT ProblemLocation {
    public:
        /**
         * Creates a problem without a specific location.
         *
         * Note that none of the possible places a location can be
         * are excluding others, even when it doesn't make sense (e.g. having a problem on a vessel *and* a container is likely a bug)
         *
         * Only one of container, stowage stack and bayrowtier should be set.
         *
         * It is also often wrong to set both container *and* call
         */
        ProblemLocation();

        /**
         * Copy constructor
         */
        ProblemLocation(const ProblemLocation& other);

        ProblemLocation& operator=(const ProblemLocation& other);
        /**
         * sets this problem's location to \param call
         */
        void setCall(const ange::schedule::Call* call);

        /**
         * \return the call this problem is attached to, or 0 if this problem isn't specific to a call
         */
        const ange::schedule::Call* call() const;

        /**
         * sets this problem location to \param container. This is for problems specific to a container
         * and isn't related to its location.
         */
        void setContainer(const ange::containers::Container* container);

        /**
         * \return the container this problem is attached to, or 0 if this problem isn't specific to a container
         */
        const ange::containers::Container* container() const;

        /**
         * sets this problem's location to \param BayRowTier. This is for problems specific to a container
         * in a specific position. For example a reefer in a non-reefer slot or a container in the space blocked
         * by a OOG.
         */
        void setBayRowTier(const ange::vessel::BayRowTier& brt);

        /**
         * \return the BayRowTier for this problem or a unplaced BayRowTier if not specific to a BayRowTier
         */
        const ange::vessel::BayRowTier& bayRowTier() const;

        /**
         * sets this problem's location to \param stackSupport. This is for problems specific to a stack support,
         * For example weight, height or lashing issues.
         */
        void setStackSupport(const ange::vessel::StackSupport* stackSupport);

        /**
         * \return the stackSupport for the problem, or 0 if not specific to a stack support
         */
        const ange::vessel::StackSupport* stackSupport() const;

        /**
         * \return if the location is legal.
         */
        bool isLegal() const;

        ~ProblemLocation();
    private:
        QSharedDataPointer<ProblemLocationData> d;
};

} // namespace angelstow
} // namespace ange


#endif // PROBLEM_LOCATION_H
