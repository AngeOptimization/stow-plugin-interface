#ifndef ANGELSTOW_STABILITYREPORTERINTERFACE_H
#define ANGELSTOW_STABILITYREPORTERINTERFACE_H

#include <QtPlugin>
#include <QString>

namespace ange {
namespace schedule {
class Call;
}
}

namespace ange {
namespace angelstow {

/**
 * The interface for the plugins that add data to the stability report
 */
class StabilityReporterInterface {

protected:
    ~StabilityReporterInterface() {};

public:
    /**
     * Return a header for the stability report. This is HTML that will be wrapped in h2 tags.
     */
    virtual QString headerForStabilityReport() = 0;

    /**
     * Return a section for the stability report in HTML. Use h3 and lesser for headers.
     */
    virtual QString sectionForStabilityReport(const ange::schedule::Call* call) = 0;

};

}
} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::StabilityReporterInterface, "dk.ange.angelstow.StabilityReporterInterface")

#endif // ANGELSTOW_STABILITYREPORTERINTERFACE_H
