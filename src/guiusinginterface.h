
#ifndef ANGELSTOW_GUIUSINGINTERFACE_H
#define ANGELSTOW_GUIUSINGINTERFACE_H

#include <QtPlugin>

namespace ange {
namespace angelstow {

class IGui;

/**
 * The interface for the plugins that interact with the GUI app
 */
class GuiUsingInterface {

protected:
    ~GuiUsingInterface() {};

public:
    /**
     * Initialization hook. This is where plugins can install menu entries, button and such.
     */
    virtual void initializeGui(IGui* gui) = 0;

};

}
} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::GuiUsingInterface, "dk.ange.angelstow.plugin.using.gui")

#endif // ANGELSTOW_GUIUSINGINTERFACE_H
