#ifndef ANGELSTOW_REASONRESERVERINTERFACE_H
#define ANGELSTOW_REASONRESERVERINTERFACE_H

namespace ange {
namespace angelstow {

class ValidatorPlugin;

/**
 * Object that manages the reasons used by the validator plugins.
 */
class ReasonReserverInterface {

public:
    /**
     * @return Reason number to be reported for validators in whyNot, and as input in reasonToString
     */
    virtual int reserveReason(ValidatorPlugin* validator_plugin) = 0;

};

}
} // namespace

Q_DECLARE_INTERFACE(ange::angelstow::ReasonReserverInterface, "dk.ange.angelstow.ReasonReserverInterface")

#endif // ANGELSTOW_REASONRESERVERINTERFACE_H
