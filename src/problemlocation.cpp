#include "problemlocation.h"

#include <QPointer>

#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/stack.h>
#include <ange/schedule/call.h>
#include <ange/vessel/stacksupport.h>

using namespace ange::vessel;
using namespace ange::containers;
using ange::schedule::Call;

namespace ange {
    namespace angelstow {

class ProblemLocationData : public QSharedData {
    public:
        QPointer<const ange::containers::Container> m_container;
        ange::vessel::BayRowTier m_brt;
        QPointer<const ange::vessel::StackSupport> m_stack_support;
        QPointer<const ange::schedule::Call> m_call;
};

const BayRowTier& ProblemLocation::bayRowTier() const {
    return d->m_brt;
}

const Call* ProblemLocation::call() const {
    return d->m_call;
}

const Container* ProblemLocation::container() const {
    return d->m_container;
}

void ProblemLocation::setContainer(const Container* container) {
    d->m_container = container;
}

ProblemLocation& ProblemLocation::operator=(const ProblemLocation& other) {
    this->d = other.d;
    return *this;
}

ProblemLocation::ProblemLocation(const ProblemLocation& other) : d(other.d) {
}

ProblemLocation::ProblemLocation() : d(new ProblemLocationData){

}

void ProblemLocation::setBayRowTier(const BayRowTier& brt) {
    d->m_brt = brt;
}

void ProblemLocation::setCall(const Call* call) {
    d->m_call = call;
}

void ProblemLocation::setStackSupport(const StackSupport* stackSupport) {
    d->m_stack_support = stackSupport;
}

const StackSupport* ProblemLocation::stackSupport() const {
    return d->m_stack_support;
}

bool ProblemLocation::isLegal() const {
    int counter = 0;
    if(d->m_stack_support) {
        counter++;
    }
    if(d->m_brt.placed()) {
        counter++;
    }
    if(d->m_container) {
        counter++;
    }
    return counter == 1 || counter == 0; //FIXME: counter used to be a bool. How would this code return anything but true?
}


ProblemLocation::~ProblemLocation() {
    // needed for QSharedDataPointer
}

    } // namespace angelstow
} // namespace ange
