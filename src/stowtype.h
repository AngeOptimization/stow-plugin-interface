#ifndef ANGE_ANGELSTOW_STOW_TYPE_H
#define ANGE_ANGELSTOW_STOW_TYPE_H

#include <QMetaType>

namespace ange {
namespace angelstow {

enum StowType {
    NonPlacedType,
    MicroStowedType,
    MasterPlannedType,
};

}
}

Q_DECLARE_METATYPE(ange::angelstow::StowType)

#endif // ANGE_ANGELSTOW_STOW_TYPE_H
